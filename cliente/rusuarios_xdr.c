/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#include "rusuarios.h"

bool_t
xdr_pareja_uids (XDR *xdrs, pareja_uids *objp)
{
	register int32_t *buf;

	 if (!xdr_int (xdrs, &objp->uid1))
		 return FALSE;
	 if (!xdr_int (xdrs, &objp->uid2))
		 return FALSE;
	return TRUE;
}

bool_t
xdr_respuesta_ugid (XDR *xdrs, respuesta_ugid *objp)
{
	register int32_t *buf;

	 if (!xdr_int (xdrs, &objp->uid))
		 return FALSE;
	 if (!xdr_int (xdrs, &objp->gid))
		 return FALSE;
	return TRUE;
}

bool_t
xdr_respuesta_nombre (XDR *xdrs, respuesta_nombre *objp)
{
	register int32_t *buf;

	 if (!xdr_bool (xdrs, &objp->existe))
		 return FALSE;
	switch (objp->existe) {
	case FALSE:
		break;
	case TRUE:
		 if (!xdr_string (xdrs, &objp->respuesta_nombre_u.nombre, ~0))
			 return FALSE;
		break;
	default:
		return FALSE;
	}
	return TRUE;
}

bool_t
xdr_arg_gids (XDR *xdrs, arg_gids *objp)
{
	register int32_t *buf;

	 if (!xdr_array (xdrs, (char **)&objp->uids.uids_val, (u_int *) &objp->uids.uids_len, ~0,
		sizeof (int), (xdrproc_t) xdr_int))
		 return FALSE;
	return TRUE;
}

bool_t
xdr_respuesta_gids (XDR *xdrs, respuesta_gids *objp)
{
	register int32_t *buf;

	 if (!xdr_array (xdrs, (char **)&objp->gids.gids_val, (u_int *) &objp->gids.gids_len, ~0,
		sizeof (int), (xdrproc_t) xdr_int))
		 return FALSE;
	return TRUE;
}

bool_t
xdr_usuario (XDR *xdrs, usuario *objp)
{
	register int32_t *buf;

	 if (!xdr_string (xdrs, &objp->nombre, ~0))
		 return FALSE;
	 if (!xdr_pointer (xdrs, (char **)&objp->sig, sizeof (usuario), (xdrproc_t) xdr_usuario))
		 return FALSE;
	return TRUE;
}

bool_t
xdr_respuesta_usuarios (XDR *xdrs, respuesta_usuarios *objp)
{
	register int32_t *buf;

	 if (!xdr_pointer (xdrs, (char **)&objp->lista, sizeof (usuario), (xdrproc_t) xdr_usuario))
		 return FALSE;
	return TRUE;
}
