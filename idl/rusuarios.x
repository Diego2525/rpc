struct pareja_uids {
	int uid1;
	int uid2;
};
struct respuesta_ugid  {
        int uid;
        int gid;
};
union respuesta_nombre switch(bool existe){
	case FALSE: void;
	case TRUE: string nombre<>;
};
struct arg_gids  {
        int uids<>;
};
struct respuesta_gids {
        int gids<>;
};
struct usuario {
        string nombre<>;
        struct usuario *sig;
};
struct respuesta_usuarios {
        struct usuario *lista;
};
program RUSUARIOS_SERVICIO {
	version RUSUARIOS_VERSION {
		int OBTENER_UID(string nombre)=1;
		respuesta_nombre OBTENER_NOMBRE(int uid)=2;
		bool MISMO_GID(pareja_uids)=3;
		respuesta_ugid OBTENER_UGID(string nombre)=4;
		respuesta_gids OBTENER_GIDS(arg_gids lista_uids)=5;
		respuesta_usuarios OBTENER_USUARIOS(void)=6;
	}=1;
}=666666666;
