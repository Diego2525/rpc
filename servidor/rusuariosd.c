/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */

#include <string.h>
#include <pwd.h>
#include <sys/types.h>
#include "rusuarios.h"

int *
obtener_uid_1_svc(char **argp, struct svc_req *rqstp)
{
	static int  result;

	/*
	 * insert server code here
	 */

	return &result;
}

respuesta_nombre *
obtener_nombre_1_svc(int *argp, struct svc_req *rqstp)
{
	static respuesta_nombre  result;

	/*
	 * insert server code here
	 */

	return &result;
}

bool_t *
mismo_gid_1_svc(pareja_uids *argp, struct svc_req *rqstp)
{
	static bool_t  result;

	/*
	 * insert server code here
	 */

	return &result;
}

respuesta_ugid *
obtener_ugid_1_svc(char **argp, struct svc_req *rqstp)
{
	static respuesta_ugid  result;

	/*
	 * insert server code here
	 */

	return &result;
}

respuesta_gids *
obtener_gids_1_svc(arg_gids *argp, struct svc_req *rqstp)
{
	static respuesta_gids  result;

	/*
	 * insert server code here
	 */

	return &result;
}

respuesta_usuarios *
obtener_usuarios_1_svc(void *argp, struct svc_req *rqstp)
{
	static respuesta_usuarios  result;
struct passwd *desc_usuario;
        struct usuario *nuevo;

        xdr_free((xdrproc_t)xdr_respuesta_usuarios, (char *)&result);

        result.lista = NULL;
        while ((desc_usuario=getpwent())) {
                nuevo = malloc(sizeof(usuario));
                nuevo->nombre = strdup(desc_usuario->pw_name);
                nuevo->sig = result.lista;
                result.lista = nuevo;
        }
        endpwent();
	/*
	 * insert server code here
	 */

	return &result;
}
