/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */
#include <time.h>
#include "temperatura.h"

int *
get_temperatura_1_svc(void *argp, struct svc_req *rqstp)
{
	static int  result;
	srand(time(NULL));
	result = rand()%30;
	return &result;
}

int *
get_humedad_1_svc(void *argp, struct svc_req *rqstp)
{
	static int  result;
        srand(time(NULL));
        result = rand()%30;
	return &result;
}

float *
get_broadcast_temperatura_1_svc(char **argp, struct svc_req *rqstp)
{
	static float  result;
	srand(time(NULL));
	result = rand()%30;
	result = result/4;
	return &result;
}

float *
get_broadcast_humedad_1_svc(char **argp, struct svc_req *rqstp)
{
	static float  result;
	srand(time(NULL));
	result = rand()%30;
	result = result/4;
	return &result;
}
